from collections import Counter
from imblearn.over_sampling import SMOTE
import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.datasets import fetch_openml
from sklearn.model_selection import train_test_split


def smote_resample(dataset_name, dataset, class_1_samples, class_2_samples):
    train_set = pd.concat(
        [dataset.where(dataset.iloc[:, -1] == "C_0").dropna().sample(class_1_samples),
         dataset.where(dataset.iloc[:, -1] == "C_1").dropna().sample(class_2_samples)]
    )
    y_train = train_set["Species"]
    x_train = train_set.drop("Species", axis=1)

    y_test = dataset["Species"]
    x_test = dataset.drop("Species", axis=1)

    smote = SMOTE()

    x_smote, y_smote = smote.fit_resample(x_train, y_train)

    print(y_smote.to_string())
    print(Counter(y_smote))

    model = LogisticRegression()
    model.fit(x_smote, y_smote)
    y_pred = model.predict(x_test)

    print(f'Accuracy ({dataset_name} {class_1_samples}-{class_2_samples}): {accuracy_score(y_test, y_pred)}')


def re_generate(dataset_name, dataset, class_1_samples, class_2_samples):
    ds1 = dataset.where(dataset.iloc[:, -1] == "C_0").dropna().sample(class_1_samples)
    train_set = dataset.where(dataset.iloc[:, -1] == "C_1").dropna().sample(class_2_samples)

    for i in range(class_2_samples - class_1_samples + 1):
        train_set = pd.concat([train_set, ds1])

    y_train = train_set["Species"]
    x_train = train_set.drop("Species", axis=1)

    y_test = dataset["Species"]
    x_test = dataset.drop("Species", axis=1)

    model = LogisticRegression()
    model.fit(x_train, y_train)
    y_pred = model.predict(x_test)

    print(f'Accuracy ({dataset_name} {class_1_samples}-{class_2_samples}): {accuracy_score(y_test, y_pred)}')


def class_weighting(dataset_name, dataset, class_1_samples, class_2_samples):
    train_set = pd.concat(
        [dataset.where(dataset.iloc[:, -1] == "C_0").dropna().sample(class_1_samples),
         dataset.where(dataset.iloc[:, -1] == "C_1").dropna().sample(class_2_samples)]
    )
    y_train = train_set["Species"]
    x_train = train_set.drop("Species", axis=1)

    y_test = dataset["Species"]
    x_test = dataset.drop("Species", axis=1)

    weights = {"C_0": len(x_train) / class_1_samples, "C_1": len(x_train) / class_2_samples}
    model = LogisticRegression(class_weight=weights)
    model.fit(x_train, y_train)
    y_pred = model.predict(x_test)

    print(f'Accuracy ({dataset_name} {class_1_samples}-{class_2_samples}): {accuracy_score(y_test, y_pred)}')


def mnist_train(dataset):
    X2_train, X2_test, y2_train, y2_test = train_test_split(dataset.data,
                                                            dataset.target.astype('int'),
                                                            test_size=1 / 7.0)
    clf2 = LogisticRegression(fit_intercept=True,
                              multi_class='auto',
                              penalty='l1',
                              solver='saga',
                              max_iter=500,
                              C=50,
                              n_jobs=5,
                              tol=0.01
                              )
    clf2.fit(X2_train, y2_train)

    print('Accuracy (Mnist) :', clf2.score(X2_test, y2_test))


def start():
    iris_df = pd.read_csv('Iris.csv')
    iris_df = iris_df.drop("Id", axis=1)

    for i in iris_df.index:
        if iris_df.at[i, 'Species'] == 'Iris-setosa':
            iris_df.at[i, 'Species'] = 'C_0'
        else:
            iris_df.at[i, 'Species'] = 'C_1'

    class_weighting('Iris-class_weighting', iris_df, 1, 99)
    class_weighting('Iris-class_weighting', iris_df, 5, 95)
    class_weighting('Iris-class_weighting', iris_df, 10, 90)
    class_weighting('Iris-class_weighting', iris_df, 20, 80)
    class_weighting('Iris-class_weighting', iris_df, 30, 70)

    re_generate('Iris-regenrate', iris_df, 1, 99)
    re_generate('Iris-regenrate', iris_df, 5, 95)
    re_generate('Iris-regenrate', iris_df, 10, 90)
    re_generate('Iris-regenrate', iris_df, 20, 80)
    re_generate('Iris-regenrate', iris_df, 30, 70)

    smote_resample('Iris-smote', iris_df, 1, 99)
    smote_resample('Iris-smote', iris_df, 6, 94)  # min sample 6
    smote_resample('Iris-smote', iris_df, 10, 90)
    smote_resample('Iris-smote', iris_df, 20, 80)
    smote_resample('Iris-smote', iris_df, 30, 70)

    mnist = fetch_openml(data_id=554, cache=True)
    mnist_train(mnist)


if __name__ == '__main__':
    start()
